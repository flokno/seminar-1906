## Strongly Anharmonic Solids
#### Anharmonicity Quantification and Heat Transport

<br/>
Florian Knoop<br/>
@size[.75em](Thomas Purcell, Matthias Scheffler, Christian Carbogno)

@snap[north-west span-40]
![](assets/img/FHIlogo.png)
@snapend

@snap[north-east span-9]
![](assets/img/FS_black.png)
@snapend


@snap[south span-100]
@size[.75em](FHI Seminar — June 3, 2019)
@snapend

---?include=parts/1/PITCHME.md
---?include=parts/heat_flux/PITCHME.md
---?include=parts/CsPbI/PITCHME.md