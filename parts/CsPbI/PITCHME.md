## How about _really_ anharmonic materials?
---

@snap[north span-100]
### CsPbI`$_3$`
@snapend
`$ \renewcommand{\b}[1]{\boldsymbol{#1}}$`

@snap[north-west]
<br/><br/><br/>
Powerful host material
@ul[](false)
- **< 1 W/mK** in **orthorombic** Phase for T < 300K  [1]
- **Cubic** phase: Efficient Photovoltaics [2]
@ulend
@snapend

@snap[south span-100 fragment]
Problem: **`$~\alpha$`-Cubic** CsPbI`$_3$` **stable above ~600K**
<br/><br/><br/><br/>
@snapend

@snap[south-east]
<p style='line-height:.9'>
<font size=5>
[1] Kovalsky _et al._, *J. Chem. Phys. C* **121**, 3228 (2017&#41; <br/>
[2] Swarnkar _et al._, *Science* **354**, 92 (2016&#41;
<font/><p/>
@snapend

@snap[midpoint span-55 fragment]
<br/><br/>
@img[shadow](assets/CsPbI3/bs_000.png)
@snapend

@snap[midpoint span-55 fragment]
<br/><br/>
@img[](assets/CsPbI3/bs_600.png)
@snapend

---

@snap[north span-100]
### CsPbI`$_3$` -- Thermal Conductivity
@snapend

@snap[midpoint span-75]
<br/>
@img[shadow](assets/kappa/kappa.png)
@snapend

@snap[south span-100 fragment]
Confirm **ultralow thermal conductivity** < 1 W / mK
<br/>
@snapend

@snap[midpoint span-75 fragment]
<br/>
@img[](assets/kappa/kappa_twochannel.png)
@snapend

---
@snap[north span-100]
### Spectral Heat Flux `$J (\omega)$`
@snapend

@snap[north-west span-100]
<br/><br/><br/>
**Fourier Transform** Heat Flux Autocorrelation function <br/>
from **time** domain:
@snapend
<br/><br/>
<br/><br/>
`$
\begin{align}
    \kappa &\propto \int \left\langle JJ \right\rangle \t d \tau \\
    &= \lim_{\omega \rightarrow 0} ~ \left| J (\omega) \right|^2
\end{align}
$`
<br/>
<br/>
Unitary transformation to **frequency space**

---
@snap[north span-100]
### Spectral Heat Flux `$J (\omega)$`
@snapend

@snap[south span-80]
@img[shadow](assets/spectra/slide.png)
@snapend

@snap[midpoint span-100 fragment]
@box[bg-white grey shadow box-padding](Heat transport in highly anharmonic system resembles <br/>heat transport in _disordered_ systems)
@snapend
---
@snap[north span-100]
### Conclusion
@snapend

@snap[midpoint span-100]
@ul(false)
- **Quantify anharmonicity** in materials
- **_Ab initio_ Green Kubo** for thermal conductivity
- Details of heat transport mechanism from **microscopic heat flux**
@ulend
@snapend

@snap[south fragment span-100]
**Thank You for your attention!**
@snapend
