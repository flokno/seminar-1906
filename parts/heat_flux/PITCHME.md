## Interlude: <br/>Heat Transport in Solids
---

@snap[north span-100]
### Green-Kubo Heat Transport
@snapend
`$ \renewcommand{\b}[1]{\boldsymbol{#1}}$`
`$ \renewcommand{\t}[1]{\text{#1}}$`

@snap[north-west span-100]
<br/><br/><br/>
Fourier's law:
@snapend
<br/>
`$
\begin{align}
    \b J &= - \kappa \b \nabla T
    \vphantom{\sum_{\b q}}
\end{align}
$`

@snap[west span-100]
<br/>
Fluctuation Dissipation Theorem:
@snapend
<br/><br/><br/>
`$
\begin{align}
    \kappa &\propto
    \int \mathrm{d} \tau ~ \left\langle \b J \, \b J \right\rangle_\textbf{eq} (\tau)
\end{align}
$`

@snap[south-west span-100]
`$\left\langle \b J \, \b J \right\rangle_\textbf{eq} (\tau)~~~$`: &nbsp; autocorrelation function of `$\b J(t)$`
<br/>
<br/>
@snapend

@snap[south-west]
@size[.6em](L. Onsager, *Phys. Rev.* **37**, 405 (1931&#41;)
@snapend
@snap[south-east]
@size[.6em](R. Kubo _et al._, *JPSJ* **12**, 1203 (1957&#41;)
@snapend

---
@snap[north span-100]
### How to define `$ \b J ~ $`?
@snapend
@snap[north-west span-100]
<br/><br/><br/>
Continuity equation for thermal energy density `$e (\b r)~~$`:
@snapend
<br/><br/><br/>
`$
\begin{align}
    \dot e (\b r, t) + \b \nabla \cdot \b j (\b r, t) = 0
\end{align}
$`
<br/><br/><br/><br/>

@snap[west span-100]
<br/>
<br/>
Integrate over entire volume to obtain `$\b J (t)~$`:
@snapend
`$
\begin{align}
    \b J (t) &= \frac{\t d}{\t d t} \int \text d^3 r ~~ \b r ~ e ( \b r, t)
\end{align}
$`
<br/>


@snap[south-east]
@size[.6em](E. Helfand, *Phys. Rev.* **119**, 1 (1960&#41;)
@snapend

---
@snap[north span-100]
### How to define `$ \b J ~ $`?
@snapend
@snap[north-west span-100]
<br/><br/><br/>
_Assume_ atomic energy partitioning for `$e (\b r)~~$`:
@snapend
<br/><br/>
`$
\begin{align}
    e (\b r, t) = \sum_I E_I(t) ~ \delta (\b r - \b R_I(t))
\end{align}
$`
<br/><br/><br/><br/>

@snap[west span-100]
<br/>
<br/>
Integrate over entire volume to obtain `$\b J~$` (again):
@snapend
@snap[south span-100]
`$
\begin{align}
    \require{cancel}
    \b J (t) &= \frac{\t d}{\t d t} \sum_I \b R_I E_I \\
    &= \sum_I \b R_I \dot E_I + \cancel{\dot{\b R}_I E_I}
\end{align}
$`
@snapend


---
@snap[north span-100]
### How to define `$ \b J ~ $`?
@snapend
@snap[north-west span-100]
<br/><br/><br/>
In **solids**, the **virial heat flux** is sufficient:
@snapend
<br/><br/>
`$
\begin{align}
    \require{cancel}
    \b J(t) &= \sum_I  \b R_I \dot E_I % + \xcancel{{\dot{\b R}_I E_I}}
\end{align}
$`

@ul[](false)
- **No** need for partitioning: Only _derivatives_ of `$E_I~$`
- **Well defined** in Density Functional Theory:
@ulend

@snap[south span-100]
`$
\begin{align}
    \b J (t) &= \sum_I \sigma_I \, \dot{\b R}_I (t)
    \\
\end{align}
$`
<br/>
<br/>
@snapend

@snap[south-east]
@size[.7em](C. Carbogno, R. Ramprasad, M. Scheffler, *PRL* **118**, 175901 (2017&#41;)
@snapend

@snap[midpoint span-100 fragment shadow bg-white]
@img[](assets/slides/page_32.png)
@snapend

@snap[midpoint span-100 fragment]
@img[](assets/slides/page_35.png)
@snapend


---
@snap[north span-100]
### _Ab initio_ Green-Kubo
@snapend
<br/>
`$
\begin{align}
    \kappa &\propto
    \int \mathrm{d} \tau ~ \left\langle \b J \, \b J \right\rangle (\tau)
\end{align}
$`
<br/> <br/>
`$
\begin{align}
    \b J (t) &= \sum_I \sigma_I \, \dot{\b R}_I (t)
\end{align}
$`
<br/>
<br/>

@snap[south span-100 bg-white]
**No approximations** to potential energy surface
<br/><br/><br/>
@snapend

@snap[north-east span-20]
@img[](assets/logos/FHIaims.png)
<br/>
<br/>
@snapend


@snap[south-east]
@size[.7em](C. Carbogno, R. Ramprasad, M. Scheffler, *PRL* **118**, 175901 (2017&#41;)
@snapend

---
@snap[north span-100]
### Boost Convergence
@snapend
@snap[north-west span-100]
<br/><br/><br/>
_Normal Mode_ Projection of Atomic Heat flux `$ \b J_I ~$`:
@snapend
<br/><br/>
<br/>
`$
\begin{align}
    \b J (t) = \sum_I \b J_I (t) &~\equiv~ \sum_{\t{commensurate }q} ~ \b J (\b q, t) \\
    &~\rightleftharpoons~ \int\limits_\t{Brillouin Zone} \b J (\b q, t) ~ \t d q
\end{align}
$`

<br/>
<br/>
Unitary transformation to **reciprocal space**

<!--
<br/>
<br/>
@ul[](false)
- `$ \b J (\b q, t) ~~$` can be _interpolated_ to _dense_ `$\b q~$` grid
- `$ \b J (\b q, t) ~~$` can be _extrapolated_ to _long times_ `$t$`
- **Overcome finite time and size constraints** in DFT
@ulend
-->

@snap[south-east]
@size[.7em](Carbogno 2017)
@snapend

@snap[midpoint span-100 fragment shadow bg-white]
@img[](assets/slides/page_43.png)
@snapend

@snap[midpoint span-100 fragment]
@img[](assets/slides/page_44.png)
@snapend

@snap[midpoint span-100 fragment]
@img[](assets/slides/page_45.png)
@snapend

@snap[midpoint span-100 fragment]
@img[](assets/slides/page_46.png)
@snapend

