from matplotlib import pyplot as plt

import numpy as np
import pandas as pd
import seaborn as sns

mat = 'MgO'

# https://artsexperiments.withgoogle.com/artpalette/colors/363639-dcceb9-a82935-5e755a-e2ad1e
cs = ["#363639", "#a82935", "#dcceb9", "#e2ad1e", "#5e755a"]

s = pd.Series.from_csv(f"{mat}.dat")

fig, ax = plt.subplots(figsize=(3.5, 2))

ax.axvline(0)
s.iloc[1:].plot(ax=ax, linewidth=2, alpha=0.3, color=cs[0])
s.iloc[1:].rolling(window=7, min_periods=0).mean().plot(
    ax=ax, linewidth=1.5, alpha=1, color=cs[1]
)

# s.rolling(window=3, min_periods=0).mean().plot(ax=ax, linewidth=3, color='k')

# s2.plot(ax=ax, linewidth=2)

ax.yaxis.set_ticklabels("")
if mat == 'CsPbI':
    ax.set_ylim([1, 75])
    ax.set_xlim([-1, 20])
elif mat == 'MgO':
    ax.set_ylim([1, 3e3])
    ax.set_xlim([-1, 25])

ax.set_ylabel("Arb. Units")
ax.set_xlabel("Frequency [THz]")

fig.tight_layout()
fig.savefig(f"{mat}.png", dpi=300)

